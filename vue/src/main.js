import Vue from 'vue'
import App from './App.vue'
import './assets/tailwind.css'
import Vuex from 'vuex'
import store from "@/store";
import i18n from './i18n'
import axios from "axios";

Vue.use(Vuex)
Vue.config.devtools = true
Vue.config.productionTip = false




new Vue({
  store,
  i18n,
  axios,
  render: h => h(App)
}).$mount('#app')


