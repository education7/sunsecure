import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        activePage: 'Home',
        popup: false,
        locale: 'ru',
        loading: false,
        dark: false,
        files: [],
        added: false,
        sites: {},

    },
    actions: {
        changePage({ commit }, page) {
            commit("changePage", page);
        },
        togglePopup({commit}) {
            commit('togglePopup')
        },
        addFile({commit}, path) {
            commit('addFile', path)
        },
        addFiles({commit}, path) {
            commit('addFiles', path)
        },
        removeFile({commit}, path) {
            commit('removeFile', path)
        },
        setSites({commit}, sites) {
            commit('setSites', sites)
        },

    },
    mutations: {
        changePage(state, page) {
            state.activePage = page
            console.log('Active page - ' + state.activePage)
        },
        togglePopup(state) {
            state.popup = !state.popup
            console.log('Popup - ' + state.popup)
        },
        addFile(state, path) {
            state.files.push({
                path: path
            })
            state.added = 'Добавлено'
            setTimeout(() => state.added = false, 3000)

        },
        addFiles(state, path) {
            state.files.push({
                path: path
            })
            state.added = 'Добавлено'
            setTimeout(() => state.added = false, 3000)
        },
        removeFile(state, path) {
            state.files = state.files.filter((file) => {
                return file.path != path
            })
            state.added = 'Удалено'
            setTimeout(() => state.added = false, 3000)

        },
        setSites(state, sites) {
            state.files =  sites
        }
    },
    getters: {
        // getEnv: (state) => state.env,
    },
});
