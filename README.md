# SunSecure - Dont worry be safe

Watch the video here: https://youtu.be/iy2aKf9AAvc



## Getting Started
- Clone the repo and cd into the directory
$ git clone 

- Open frontend folder `vue`
`$ yarn install` - install dependencies
`$ yarn build` - build project
`$ yarn serve` - dev mode


- After build, go to main directory and run Python script


## Requirement
- `Install eel, python`
- `Install yarn/npm`


## Run the app

```sh
$ python main.py

## Packaging the app
You can pass any valid `pyinstaller` flag in the following command to further customize the way your app is built.
```sh
$ python -m eel main.py web --noconsole --onefile --icon=barcode.icns
```
