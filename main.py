import eel
import json
import requests
import time
import pyclamd
import iptc
from elevate import elevate
import sqlite3
import os
import socket


conn = sqlite3.connect('malware_sites.db')
cursor = conn.cursor()

try:
    # for Python2
    from Tkinter import *   ## notice capitalized T in Tkinter
except ImportError:
    # for Python3
    from tkinter import *   ## notice lowercase 't' in tkinter here

from tkinter import filedialog

scan_url = 'https://www.virustotal.com/vtapi/v2/url/report'

# this function takes the path to the file and then asks VirusTotal to scan it
@eel.expose
def site_scanning(site_url):
    params = dict(apikey='a8f5b80d6293c4f3f7552aad48c9a2441c2207699305631fd6548ead3e32f22d')
    params = {'apikey': 'a8f5b80d6293c4f3f7552aad48c9a2441c2207699305631fd6548ead3e32f22d', 'resource':site_url}
    response = requests.get(scan_url, params=params)
    print(response)
    if response.status_code == 200:
        scan_result = response.json()
        print(scan_result)
        resource = scan_result['resource']
        print(resource)
        return scan_result


@eel.expose
def get_file():
    root = Tk()
    root.withdraw()
    root.wm_attributes('-topmost', 1)
    filename = filedialog.askopenfilename()
    print(filename)
    return filename


@eel.expose
def file_scanning(path_to_file):
    cd = pyclamd.ClamdUnixSocket()
    result = cd.scan_file(path_to_file)
    print(result)
    time.sleep(1)
    return result

@eel.expose
def file_delete(path_to_file):
    os.remove(path_to_file)
    print("File Removed!")
    return 'Removed'


@eel.expose
def get_folder():
    root = Tk()
    root.withdraw()
    root.wm_attributes('-topmost', 1)
    directory = filedialog.askdirectory()
    print(directory)
    return directory


@eel.expose
def folder_scanning(path_to_folder):
    cd = pyclamd.ClamdUnixSocket()
    result = cd.multiscan_file(path_to_folder)
    print(result)
    time.sleep(1)
    return result



# a function that filters all packages
def package_filter(address, action):
    iptb_chain = iptc.Chain(iptc.Table(iptc.Table.FILTER), 'OUTPUT')  # chains: INPUT, OUTPUT, FORWARD
    print('1')
    rule = iptc.Rule()
    print('1')
#     action = int(action)
    if action == 'DROP':
        target = iptc.Target(rule, 'DROP')
        rule.target = target
    elif action == 'ACCEPT':
        target = iptc.Target(rule, 'ACCEPT')
        rule.target = target
    elif action == 'REJECT':
        target = iptc.Target(rule, 'REJECT')
        rule.target = target
    elif action == 'LOG':
        target = iptc.Target(rule, 'LOG')
        rule.target = target
    else:
        print('You have to choose the action.')
    if chain == 'INPUT':
        print('inp')
        rule.src = ip
    elif chain == 'OUTPUT':
        print('out')
        rule.dst = ip
    else:
        pass

    print(rule)
    elevate()
    iptb_chain.insert_rule(rule)
    return show_all_records()


# that function inserts new values (a malware site and its address) into table
@eel.expose
def insert_new_record(address, action):
    print(action)
    print(address)
    cursor.execute("INSERT INTO blocked_sites VALUES (?, ?)", (address, action))
    conn.commit()
#     package_filter(address, action)
    return get_all_records()



@eel.expose
def get_all_records():
    conn = sqlite3.connect('malware_sites.db')
    conn.row_factory = sqlite3.Row # This enables column access by name: row['column_name']
    rows = conn.cursor()

    rows = conn.execute('''
    SELECT * from blocked_sites
    ''').fetchall()
    conn.commit()
    conn.close()
    return json.dumps( [dict(ix) for ix in rows] ) #CREATE JSON


# def get_all_records():
#     rows = cursor.execute('''
#     SELECT * from blocked_sites
#     ''').fetchall()
#     conn.commit()
#     result = json.dumps(row, indent=2)
#
#     return rows





def take_address(address_name):
    for row in cursor.execute('''SELECT address FROM blocked_sites WHERE name=(?)''', (address_name,)):
        return row[0]
    conn.commit()

@eel.expose
def delete_records(address):
#     cursor.execute('''DELETE FROM blocked_sites WHERE address=(?)''', (address))
    cursor.execute("DELETE FROM blocked_sites WHERE address=(?)", (address,))
    conn.commit()
    action = "ACCEPT"
#     package_filter(address, action)

    return get_all_records()


# cursor.execute('''CREATE TABLE blocked_sites (address text, action text)''')


eel.init('web')
eel.start('index.html', size=(1366, 800))
